import random
import string
import base64
import hashlib

from functools import partial

def genAuid(realm, randomLen):
    auidRandomBase = "".join((string.ascii_letters, string.digits))
    auidRandom = "".join(random.SystemRandom().choice(auidRandomBase) for _ in range(randomLen))
    return "-".join(("AUID", realm, auidRandom))

def genAuidFactory(realm = "DEFAULT", randomLen = 48):
    return partial(genAuid, realm, randomLen)

def getDecodedKeyData(key):
    pubKey = key.split()[1]
    return base64.standard_b64decode(pubKey)

def genKeyFingerMD5(key):
    pubKey = getDecodedKeyData(key)
    return hashlib.md5(pubKey).hexdigest()

def genKeyFingerSHA256(key):
    pubKey = getDecodedKeyData(key)
    digest = hashlib.sha256(pubKey).digest()
    b64digest = base64.standard_b64encode(digest)
