import falcon
import json

from sqlalchemy import create_engine
from .resources import GroupsResource, UsersResource

with open("config.json") as f:
    config = json.load(f)
dbUser     = config.get("dbUser", "Ausweis")
dbPassword = config.get("dbPassword", "Ausweis")
dbHost     = config.get("dbHost", "localhost")
dbName     = config.get("dbName", "ausweis")
dbUri      = f"mysql+pymysql://{dbUser}:{dbPassword}@{dbHost}/{dbName}"
dbEngine   = create_engine(dbUri)

application = falcon.API()
application.add_route("/groups", GroupsResource(dbEngine))
application.add_route("/users", UsersResource(dbEngine))
